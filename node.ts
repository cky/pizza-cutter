// Rough JS port of https://gitlab.com/cky/shunting-yard/-/blob/main/src/common.rs
// Can't be bothered to port the test, you'll just have to trust it's correct. :-P

import { BinaryFunction, UnaryFunction, Value } from "./operator.ts";
import { DisplayMode } from "./three-faced.ts";

export interface Node {
    evaluate(): Value;
    toString(displayMode: DisplayMode): string;
}

export class LiteralNode implements Node {
    #value: Value;

    constructor(value: Value) {
        this.#value = value;
    }

    evaluate(): Value {
        return this.#value;
    }

    toString(displayMode: DisplayMode): string {
        return this.#value.toString(displayMode);
    }
}

export class UnaryNode implements Node {
    #name: string;
    #func: UnaryFunction;
    #args: [Node];

    constructor(name: string, func: UnaryFunction, a: Node) {
        this.#name = name;
        this.#func = func;
        this.#args = [a];
    }

    evaluate(): Value {
        const [a] = this.#args;
        return this.#func(a.evaluate());
    }

    toString(displayMode: DisplayMode): string {
        const [a] = this.#args;
        return `${this.#name}${a.toString(displayMode)}`;
    }
}

export class BinaryNode implements Node {
    #name: string;
    #func: BinaryFunction;
    #args: [Node, Node];

    constructor(name: string, func: BinaryFunction, a: Node, b: Node) {
        this.#name = name;
        this.#func = func;
        this.#args = [a, b];
    }

    evaluate(): Value {
        const [a, b] = this.#args;
        return this.#func(a.evaluate(), b.evaluate());
    }

    toString(displayMode: DisplayMode): string {
        const [a, b] = this.#args;
        return `(${a.toString(displayMode)} ${this.#name} ${b.toString(displayMode)})`;
    }
}
