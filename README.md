## Pizza Cutter

Pizza Cutter is a simple tool for examining how a computer processes floating-point numbers. The name is based on
[an article I wrote] on the subject of why `0.1 + 0.2 != 0.3` in binary floating-point formats.

### Usage

```
deno run https://gitlab.com/cky/pizza-cutter/-/raw/main/main.ts
```

Just enter expressions (only `+`, `-`, `*`, `/`, and `**` operators are supported) and see how they get computed in
different formats.

Use `/mode` to change the format in effect:

- `double` (default): compute using double-precision (binary64) format, display in decimal.
- `doublehex`: compute using binary64, display in hex.
- `single`: compute using single-precision (binary32) format, display in decimal.
- `singlehex`: compute using binary32, display in hex.
- `big`: compute using arbitrary-precision numbers, display in decimal.

[an article I wrote]: https://qr.ae/mDcAq
