import { readLines } from "https://deno.land/std@0.137.0/io/mod.ts";
import { lex } from "./lexer.ts";
import { Node } from "./node.ts";
import { Value } from "./operator.ts";
import { parse } from "./shunting-yard.ts";
import { DisplayMode, isDisplayMode } from "./three-faced.ts";

let displayMode: DisplayMode = "double";
let expression: Node | null = null;
let result: Value | null = null;

console.log("Enter expressions to compute. Set the display mode using the /mode command.");
console.log("Valid modes are single, singlehex, double, doublehex, and big.");
for await (const line of readLines(Deno.stdin)) {
    if (slash(line)) {
        continue;
    }
    try {
        expression = parse(lex(line));
    } catch (e) {
        if (e instanceof SyntaxError) {
            console.warn(`Can't parse ${line}: ${e.message}`);
            continue;
        } else {
            throw e;
        }
    }
    try {
        result = expression.evaluate();
    } catch (e) {
        if (e instanceof Error) {
            console.warn(`Can't evaluate ${expression.toString(displayMode)}: ${e.message}`);
            continue;
        } else {
            throw e;
        }
    }
    printExpressionAndResult();
}

function slash(line: string): boolean {
    if (!line.startsWith("/")) {
        return false;
    }
    const [command, ...args] = line.substring(1).split(/\s+/);
    switch (command) {
        case "mode":
            mode(...args);
            break;
        default:
            console.warn(`Invalid command ${command}`);
            break;
    }
    return true;
}

function mode(newMode?: string): void {
    if (newMode === undefined) {
        console.info(`Current display mode is ${displayMode}`);
    } else {
        if (isDisplayMode(newMode)) {
            displayMode = newMode;
            console.info(`Display mode set to ${newMode}`);
            printExpressionAndResult();
        } else {
            console.warn(`Invalid display mode ${newMode}`);
        }
    }
}

function printExpressionAndResult(): void {
    if (expression !== null) {
        console.info(`Expression: ${expression.toString(displayMode)}`);
    }
    if (result !== null) {
        console.info(`Result: ${result.toString(displayMode)}`);
    }
}
