import { BigDenary } from "https://deno.land/x/bigdenary@1.0.0/mod.ts";
import {
    coerceToSingle,
    doubleToBigDenary,
    doubleToHex,
    hexToBigDenary,
    hexToDouble,
    singleToBigDenary,
    singleToHex,
} from "./float-parser.ts";

export type DisplayMode = "single" | "singlehex" | "double" | "doublehex" | "big";

export class ThreeFaced {
    single: number;
    double: number;
    big: BigDenary;

    constructor(single: number, double: number, big: BigDenary) {
        this.single = single;
        this.double = double;
        this.big = big;
    }

    toString(displayMode: DisplayMode): string {
        switch (displayMode) {
            case "single":
                return singleToBigDenary(this.single).toString();
            case "singlehex":
                return singleToHex(this.single);
            case "double":
                return doubleToBigDenary(this.double).toString();
            case "doublehex":
                return doubleToHex(this.double);
            case "big":
                return this.big.toString();
        }
    }

    static fromDecimal(decimal: string): ThreeFaced {
        const float = Number.parseFloat(decimal);
        return new ThreeFaced(coerceToSingle(float), float, new BigDenary(decimal));
    }

    static fromHex(hex: string): ThreeFaced {
        const float = hexToDouble(hex);
        return new ThreeFaced(coerceToSingle(float), float, hexToBigDenary(hex)!);
    }

    static neg(a: ThreeFaced): ThreeFaced {
        return new ThreeFaced(
            -a.single,
            -a.double,
            new BigDenary({
                base: -a.big.base,
                decimals: a.big.decimals,
            }),
        );
    }

    static add(a: ThreeFaced, b: ThreeFaced): ThreeFaced {
        return new ThreeFaced(
            coerceToSingle(a.single + b.single),
            a.double + b.double,
            a.big.add(b.big),
        );
    }

    static sub(a: ThreeFaced, b: ThreeFaced): ThreeFaced {
        return new ThreeFaced(
            coerceToSingle(a.single - b.single),
            a.double - b.double,
            a.big.sub(b.big),
        );
    }

    static mul(a: ThreeFaced, b: ThreeFaced): ThreeFaced {
        return new ThreeFaced(
            coerceToSingle(a.single * b.single),
            a.double * b.double,
            a.big.mul(b.big),
        );
    }

    static div(a: ThreeFaced, b: ThreeFaced): ThreeFaced {
        return new ThreeFaced(
            coerceToSingle(a.single / b.single),
            a.double / b.double,
            a.big.div(b.big),
        );
    }

    static pow(a: ThreeFaced, b: ThreeFaced): ThreeFaced {
        if (b.big.decimals !== 0) {
            throw new RangeError("fractional powers currently not supported");
        }
        return new ThreeFaced(
            coerceToSingle(a.single ** b.single),
            a.double ** b.double,
            new BigDenary({
                base: a.big.base ** b.big.base,
                decimals: a.big.decimals * Number(b.big.base),
            }),
        );
    }
}

export function isDisplayMode(value: string): value is DisplayMode {
    switch (value) {
        case "single":
        case "singlehex":
        case "double":
        case "doublehex":
        case "big":
            return true;
        default:
            return false;
    }
}
