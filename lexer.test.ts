import { assertEquals, assertThrows } from "https://deno.land/std@0.137.0/testing/asserts.ts";
import { lex, Token } from "./lexer.ts";

Deno.test(function testLex() {
    function assertNext(iter: Iterator<Token>, token: Token) {
        assertEquals(iter.next().value, token);
    }

    const iter = lex("0x1p0 + (2 * 3.1415926535)**4");
    assertNext(iter, ["hex", "0x1p0"]);
    assertNext(iter, ["op", "+"]);
    assertNext(iter, ["left"]);
    assertNext(iter, ["dec", "2"]);
    assertNext(iter, ["op", "*"]);
    assertNext(iter, ["dec", "3.1415926535"]);
    assertNext(iter, ["right"]);
    assertNext(iter, ["op", "**"]);
    assertNext(iter, ["dec", "4"]);

    assertThrows(
        function () {
            for (const _ of lex("0x1p0 % 2")) {
                // ignore
            }
        },
        SyntaxError,
        "unknown character %",
    );
});
