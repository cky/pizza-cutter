// Rough JS port of https://gitlab.com/cky/shunting-yard/-/blob/main/src/infix.rs
// Can't be bothered to port the test, you'll just have to trust it's correct. :-P

import { Token } from "./lexer.ts";
import { BinaryNode, LiteralNode, Node, UnaryNode } from "./node.ts";
import { BinaryOperator, getBinary, getUnary, hasHigherPrecedence, Operator, UnaryOperator } from "./operator.ts";
import { ThreeFaced } from "./three-faced.ts";

type TokenState = "Initial" | "HasNumber";

class ParseState {
    #state: TokenState;
    #operatorStack: Array<Operator>;
    #operandStack: Array<Node>;

    constructor() {
        this.#state = "Initial";
        this.#operatorStack = [];
        this.#operandStack = [];
    }

    feed(token: Token): void {
        switch (this.#state) {
            case "Initial":
                return this.#feedInitial(token);
            case "HasNumber":
                return this.#feedHasNumber(token);
        }
    }

    result(): Node {
        this.#popTail();
        if (this.#operatorStack.length === 0 && this.#operandStack.length === 1) {
            return this.#operandStack.pop()!;
        } else {
            throw new Error("invalid state");
        }
    }

    #feedInitial(token: Token): void {
        switch (token[0]) {
            case "dec":
                return this.#pushDecimalLiteral(token[1]);
            case "hex":
                return this.#pushHexLiteral(token[1]);
            case "left":
                return this.#pushParen();
            case "op": {
                const unary = getUnary(token[1]);
                if (unary) {
                    return this.#pushUnary(unary);
                } else {
                    throw new SyntaxError("unexpected operator");
                }
            }
            default:
                throw new SyntaxError("unexpected token");
        }
    }

    #feedHasNumber(token: Token): void {
        switch (token[0]) {
            case "right":
                return this.#popParen();
            case "op": {
                const binary = getBinary(token[1]);
                if (binary) {
                    return this.#pushBinary(binary);
                } else {
                    throw new SyntaxError("unexpected operator");
                }
            }
            default:
                throw new SyntaxError("unexpected token");
        }
    }

    #pushParen(): void {
        this.#operatorStack.push(["paren"]);
    }

    #pushDecimalLiteral(literal: string): void {
        this.#operandStack.push(new LiteralNode(ThreeFaced.fromDecimal(literal)));
        this.#state = "HasNumber";
    }

    #pushHexLiteral(literal: string): void {
        this.#operandStack.push(new LiteralNode(ThreeFaced.fromHex(literal)));
        this.#state = "HasNumber";
    }

    #pushUnary(unary: UnaryOperator): void {
        this.#operatorStack.push(["unary", unary]);
    }

    #pushBinary(binary: BinaryOperator): void {
        this.#popHigherPrecedence(binary);
        this.#operatorStack.push(["binary", binary]);
        this.#state = "Initial";
    }

    #popHigherPrecedence(binary: BinaryOperator): void {
        while (true) {
            const lastOp = this.#operatorStack.pop();
            if (!lastOp) {
                return;
            }
            switch (lastOp[0]) {
                case "unary":
                    this.#processUnary(lastOp[1]);
                    break;
                case "binary":
                    if (hasHigherPrecedence(lastOp[1], binary)) {
                        this.#processBinary(lastOp[1]);
                        break;
                    }
                    // fallthrough
                default:
                    this.#operatorStack.push(lastOp);
                    return;
            }
        }
    }

    #popParen(): void {
        while (true) {
            const op = this.#operatorStack.pop();
            if (!op) {
                throw new SyntaxError("unmatched parenthesis");
            }
            switch (op[0]) {
                case "paren":
                    return;
                case "unary":
                    this.#processUnary(op[1]);
                    break;
                case "binary":
                    this.#processBinary(op[1]);
                    break;
            }
        }
    }

    #popTail(): void {
        if (this.#state !== "HasNumber") {
            throw new SyntaxError("unexpected end of input");
        }

        while (true) {
            const op = this.#operatorStack.pop();
            if (!op) {
                return;
            }
            switch (op[0]) {
                case "paren":
                    throw new SyntaxError("unmatched parenthesis");
                case "unary":
                    this.#processUnary(op[1]);
                    break;
                case "binary":
                    this.#processBinary(op[1]);
                    break;
            }
        }
    }

    #processUnary(op: UnaryOperator): void {
        const value = this.#operandStack.pop();
        if (value === undefined) {
            throw new Error("invalid state");
        }
        this.#operandStack.push(new UnaryNode(op[0], op[1], value));
    }

    #processBinary(op: BinaryOperator): void {
        const rhs = this.#operandStack.pop();
        const lhs = this.#operandStack.pop();
        if (rhs === undefined || lhs === undefined) {
            throw new Error("invalid state");
        }
        this.#operandStack.push(new BinaryNode(op[0], op[1], lhs, rhs));
    }
}

export function parse(tokens: Iterable<Token>): Node {
    const parser = new ParseState();
    for (const token of tokens) {
        parser.feed(token);
    }
    return parser.result();
}
