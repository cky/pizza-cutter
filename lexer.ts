const pattern =
    /(?<hex>-?0x(?:[\da-f]+\.?[\da-f]*|\.[\da-f]+)p[-+]?\d+)|(?<dec>-?(?:(?:0|[1-9]\d*)\.?\d*|\.\d+)(?:e[-+]?\d+)?)|(?<left>\()|(?<right>\))|(?<op>\*\*|[-+*/])|(?<ws>\s+)|(?<other>.)/gi;

export type Token =
    | [tag: "hex", value: string]
    | [tag: "dec", value: string]
    | [tag: "left"]
    | [tag: "right"]
    | [tag: "op", value: string];

export function* lex(str: string): IterableIterator<Token> {
    for (const match of str.matchAll(pattern)) {
        const groups = match.groups;
        if (!groups) {
            continue;
        } else if (groups.hex) {
            yield ["hex", groups.hex];
        } else if (groups.dec) {
            yield ["dec", groups.dec];
        } else if (groups.left) {
            yield ["left"];
        } else if (groups.right) {
            yield ["right"];
        } else if (groups.op) {
            yield ["op", groups.op];
        } else if (groups.other) {
            throw new SyntaxError(`unknown character ${groups.other}`);
        }
    }
}
