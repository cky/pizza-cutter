// Rough JS port of https://gitlab.com/cky/shunting-yard/-/blob/main/src/common.rs
// Can't be bothered to port the test, you'll just have to trust it's correct. :-P

import { ThreeFaced } from "./three-faced.ts";

export type Operator =
    | [tag: "paren"]
    | [tag: "unary", value: UnaryOperator]
    | [tag: "binary", value: BinaryOperator];

export type UnaryOperator = [name: string, func: UnaryFunction];
export type BinaryOperator = [name: string, func: BinaryFunction, precedence: Precedence, associativity: Associativity];

export type Precedence = number;
export type Associativity = "left" | "right";

export type Value = ThreeFaced;
export type UnaryFunction = (a: Value) => Value;
export type BinaryFunction = (a: Value, b: Value) => Value;

export function getUnary(name: string): UnaryOperator | null {
    switch (name) {
        case "-":
            return ["-", ThreeFaced.neg];
        default:
            return null;
    }
}

export function getBinary(name: string): BinaryOperator | null {
    switch (name) {
        case "+":
            return ["+", ThreeFaced.add, 0, "left"];
        case "-":
            return ["-", ThreeFaced.sub, 0, "left"];
        case "*":
            return ["*", ThreeFaced.mul, 1, "left"];
        case "/":
            return ["/", ThreeFaced.div, 1, "left"];
        case "**":
            return ["**", ThreeFaced.pow, 2, "right"];
        default:
            return null;
    }
}

export function hasHigherPrecedence(a: BinaryOperator, b: BinaryOperator): boolean {
    if (a[2] === b[2]) {
        return a[3] === "left" && b[3] === "left";
    } else {
        return a[2] > b[2];
    }
}
