import { sprintf } from "https://deno.land/std@0.137.0/fmt/printf.ts";
import { BigDenary } from "https://deno.land/x/bigdenary@1.0.0/mod.ts";

interface Components {
    sign: boolean;
    exponent: number;
    significand: bigint;
}

interface HexFloatParameters {
    sign: boolean;
    left: number;
    right: number;
    rightWidth: number;
    exponent: number;
}

export function doubleToBigDenary(value: number): BigDenary {
    return componentsToBigDenary(normaliseComponents(doubleToComponents(value)));
}

export function singleToBigDenary(value: number): BigDenary {
    return componentsToBigDenary(normaliseComponents(singleToComponents(value)));
}

export function doubleToHex(value: number): string {
    return commonToHex(doubleToHexFloatParameters(value));
}

export function singleToHex(value: number): string {
    return commonToHex(singleToHexFloatParameters(value));
}

export function hexToDouble(hex: string): number {
    const components = hexToComponents(hex);
    if (!components) {
        return Number.NaN;
    }
    const packed = packComponents(11, 52, components);
    if (packed === null) {
        return Number.NaN;
    }
    return longBitsToDouble(packed);
}

export function hexToSingle(hex: string): number {
    const components = hexToComponents(hex);
    if (!components) {
        return Number.NaN;
    }
    const packed = packComponents(8, 23, components);
    if (packed === null) {
        return Number.NaN;
    }
    return intBitsToSingle(Number(packed));
}

export function hexToBigDenary(hex: string): BigDenary | null {
    const components = hexToComponents(hex);
    if (!components) {
        return null;
    }
    return componentsToBigDenary(normaliseComponents(components));
}

export function doubleNextUp(value: number): number {
    if (value !== value || value === Number.POSITIVE_INFINITY) {
        return value;
    }
    return longBitsToDouble(doubleToLongBits(value) + 1n);
}

export function doubleNextDown(value: number): number {
    if (value !== value || value === Number.NEGATIVE_INFINITY) {
        return value;
    }
    return longBitsToDouble(doubleToLongBits(value) - 1n);
}

export function singleNextUp(value: number): number {
    if (value !== value || value === Number.POSITIVE_INFINITY) {
        return value;
    }
    return intBitsToSingle(singleToIntBits(value) + 1);
}

export function singleNextDown(value: number): number {
    if (value !== value || value === Number.NEGATIVE_INFINITY) {
        return value;
    }
    return intBitsToSingle(singleToIntBits(value) - 1);
}

export function coerceToSingle(value: number): number {
    const buffer = new ArrayBuffer(4);
    const view = new DataView(buffer);
    view.setFloat32(0, value);
    return view.getFloat32(0);
}

export function doubleToLongBits(value: number): bigint {
    const buffer = new ArrayBuffer(8);
    const view = new DataView(buffer);
    view.setFloat64(0, value);
    return view.getBigUint64(0);
}

export function longBitsToDouble(bits: bigint): number {
    const buffer = new ArrayBuffer(8);
    const view = new DataView(buffer);
    view.setBigUint64(0, bits & 0xFFFFFFFFFFFFFFFFn);
    return view.getFloat64(0);
}

export function singleToIntBits(value: number): number {
    const buffer = new ArrayBuffer(4);
    const view = new DataView(buffer);
    view.setFloat32(0, value);
    return view.getUint32(0);
}

export function intBitsToSingle(bits: number): number {
    const buffer = new ArrayBuffer(4);
    const view = new DataView(buffer);
    view.setUint32(0, bits);
    return view.getFloat32(0);
}

function doubleToComponents(value: number): Components {
    return packedToComponents(11, 52, doubleToLongBits(value));
}

function singleToComponents(value: number): Components {
    return packedToComponents(8, 23, BigInt(singleToIntBits(value)));
}

function packedToComponents(expWidth: number, sigWidth: number, packed: bigint): Components {
    let exponent = Number(packed >> BigInt(sigWidth)) & ((1 << expWidth) - 1);
    let significand = packed & ((1n << BigInt(sigWidth)) - 1n);
    const maxExp = (1 << expWidth) - 1;
    const expBias = maxExp >> 1;
    const adjustment = expBias + sigWidth;
    if (exponent === maxExp) {
        throw RangeError("Infinity or NaN");
    }
    if (!exponent) {
        exponent = -adjustment + 1;
    } else {
        significand += 1n << BigInt(sigWidth);
        exponent -= adjustment;
    }
    return { sign: Boolean(packed >> BigInt(expWidth + sigWidth)), exponent, significand };
}

function ilog2(value: bigint): number {
    let last = Number.NEGATIVE_INFINITY;
    let base = 0;
    while (value) {
        const word = Number(value & 0xFFFFFFFFn);
        const pos = 31 - Math.clz32(word);
        value >>= 32n;
        last = base + pos;
        base += 32;
    }
    return last;
}

function ctz(value: bigint): number {
    if (!value) {
        return Number.POSITIVE_INFINITY;
    }
    let count = 0;
    while (true) {
        const word = Number(value & 0xFFFFFFFFn);
        const ctz = 32 - Math.clz32(~word & (word - 1));
        count += ctz;
        if (ctz < 32) {
            return count;
        }
        value >>= 32n;
    }
}

function normaliseComponents({ sign, exponent, significand }: Components): Components {
    if (!significand) {
        return { sign, exponent: 0, significand };
    }
    const count = ctz(significand);
    return { sign, exponent: exponent + count, significand: significand >> BigInt(count) };
}

function componentsToBigDenary({ sign, exponent, significand }: Components): BigDenary {
    if (sign) {
        significand = -significand;
    }
    if (exponent >= 0) {
        return new BigDenary(significand << BigInt(exponent));
    } else {
        return new BigDenary({ base: significand * 5n ** BigInt(-exponent), decimals: -exponent });
    }
}

function doubleToHexFloatParameters(value: number): HexFloatParameters {
    const { sign, exponent, significand } = doubleToComponents(value);
    return {
        sign,
        left: Number(significand >> 52n),
        right: Number(significand & 0xFFFFFFFFFFFFFn),
        rightWidth: 13,
        exponent: exponent + 52,
    };
}

function singleToHexFloatParameters(value: number): HexFloatParameters {
    const { sign, exponent, significand } = singleToComponents(value);
    return {
        sign,
        left: Number(significand) >> 23,
        right: (Number(significand) << 1) & 0xFFFFFF,
        rightWidth: 6,
        exponent: exponent + 23,
    };
}

function commonToHex({ sign, left, right, rightWidth, exponent }: HexFloatParameters): string {
    const digits = sprintf("%0*x", rightWidth, right).replace(/0+$/, "");
    const isZero = !left && !right;
    return sprintf("%s%#x%s%sp%d", sign ? "-" : "", left, digits ? "." : "", digits, isZero ? 0 : exponent);
}

const hexPattern = /^(?<sign>-?)0x(?<lhs>[\da-f]*)\.?(?<rhs>[\da-f]*)p(?<exp>[-+]?\d+)$/i;

function hexToComponents(hex: string): Components | null {
    const match = hexPattern.exec(hex);
    if (match === null) {
        return null;
    }
    const { sign, lhs, rhs, exp } = match.groups!;

    const digits = `${lhs}${rhs}`;
    if (!digits) {
        return null;
    }
    return {
        sign: Boolean(sign),
        exponent: Number(exp) - 4 * rhs.length,
        significand: BigInt(`0x${digits}`),
    };
}

function packComponents(expWidth: number, sigWidth: number, components: Components): bigint | null {
    let { sign, exponent, significand } = normaliseComponents(components);
    if (!significand) {
        return BigInt(sign) << BigInt(expWidth + sigWidth);
    }
    const log = ilog2(significand);
    let shift = sigWidth - log;
    if (shift < 0) {
        return null;
    }
    const maxExp = (1 << expWidth) - 1;
    const expBias = maxExp >> 1;
    exponent += expBias + sigWidth - shift;
    if (exponent >= maxExp) {
        return null;
    }
    if (exponent < 0) { // subnormal
        shift += exponent - 1;
        if (shift < 0) {
            return null;
        }
        exponent = 0;
    } else {
        significand -= 1n << BigInt(log);
    }
    return (BigInt(sign) << BigInt(expWidth + sigWidth)) +
        (BigInt(exponent) << BigInt(sigWidth)) +
        (significand << BigInt(shift));
}
